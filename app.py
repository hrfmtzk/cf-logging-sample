#!/usr/bin/env python3

from aws_cdk import core

from cf_logging.cf_logging_stack import CfLoggingStack


app = core.App()
CfLoggingStack(app, "cf-logging")

app.synth()
